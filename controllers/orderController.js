const Order = require("../models/Order");
const Product = require("../models/Product");
const mongoose = require("mongoose");



// Non-admin User checkout (Create Order)
module.exports.createOrder = async (data) => {

	if(data.isAdmin){

		let message = Promise.resolve('Only Non-Admin Users can access this!');
		return message.then((value) => 
			{return {value};
		});

	 } else {

		const products = data.body.products;
		let totalAmount = 0;
		let newOrder = new Order({
			userId: data.userId,
			products: []
		});

		for (const product of products) {
			const retrieveProduct = await Product.findById(product.productId);
			const addProduct = {
				productId: retrieveProduct._id,
				productName: retrieveProduct.name,
				quantity: product.quantity,
				subTotal: product.quantity * retrieveProduct.price,
			};
			
			totalAmount += addProduct.subTotal;
			newOrder.products.push(addProduct);	
		};
		
		newOrder.totalAmount = totalAmount;
		return newOrder.save().then((savedOrder) => savedOrder);
	
		};
};



// Retrieve an authenticated user's orders
module.exports.getOrder = (data) => {
	return Order.find({userId: data.id}).then(result => {
		return result;
	});
};


// Retrieve all Orders
module.exports.getAllOrders = (data) => {
	if(data.isAdmin){
		return Order.find().then(result => {
			return result;
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};